import numpy as np
import multiprocessing

import genetcache as gnc
from generators import scalefreegen as sfg

netSize = 5000
numReplicates = 10
startBeta = 0.05
endBeta = 0.99
stepSizeBeta = 0.05
deltaIn = 0.1
deltaOut = 0.1

def generateNetwork(id, beta):
    np.random.seed()
    net = sfg.GenerateScaleFreeNetworkByBeta(netSize, beta, deltaIn, deltaOut)
    print(f"Generated {id}: N:{netSize} B:{beta} dIn:{deltaIn} dOut:{deltaOut}")
    gnc.WriteToFile(net, f"./outputs/Net_DSF_{id}_N{netSize}_B{beta}_dIn{deltaIn}_dOut{deltaOut}.csv")
    print(f"Files Written {id}: N{netSize} B:{beta} dIn:{deltaIn} dOut:{deltaOut}")
    return net

if __name__ == '__main__':
    outputs = []
    with multiprocessing.Pool(max(multiprocessing.cpu_count() - 1, 1)) as p:
        paramList = [(i, b) for i,b in enumerate( np.repeat( np.arange(startBeta, endBeta, stepSizeBeta), numReplicates) )]
        outputs = p.starmap(generateNetwork, paramList)

# HOut = np.histogram(net1['outDegree'])
# HIn = np.histogram(net1['inDegree'])

# print(HIn)

# gnc.DrawHistogram(HIn,'In Degree')
# gnc.DrawHistogram(HOut, 'Out Degree')
