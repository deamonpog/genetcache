import matplotlib.pyplot as plt
import numpy as np
import os

def DrawHistogram(numpyHistogram, thexlabel):
    plt.bar(numpyHistogram[1][:-1], numpyHistogram[0], width=np.diff(numpyHistogram[1]), ec='k')
    txw = np.diff(numpyHistogram[1])[0]
    for i,v in enumerate(numpyHistogram[0]):
        plt.text(i * txw - txw / 2, v + 5, str(v), color='blue', fontweight='bold')
    temp = np.arange(int(numpyHistogram[1].max()))
    plt.xticks(temp, temp)
    plt.xlabel(thexlabel)
    plt.ylabel('Frequency')
    plt.show()

def WriteToFile(net, filePathAndName):
    with open(filePathAndName, "w") as file:
        file.write("src,dst\n")
        for src in range(net['size']):
            for dst in range(net['size']):
                if net['edges'][src,dst] != 0:
                    file.write(f"{src},{dst}\n")
